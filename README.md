# Rocket Trail

This is a little game I made in under 48 hours for a game jam titled “Game Maker’s Toolkit Game Jam 2018”. It is written in C# and uses the Unity Engine. The user-rated and somewhat arbitrary “rank” was 142 out of 1051.

[Play it here!](https://alex-src.itch.io/rocket-trail)